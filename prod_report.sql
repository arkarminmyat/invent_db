-- ORDER SUMMARY
CREATE VIEW ivtv_order_summary 
AS
SELECT 
OrderID,TUnitPrice,TUnitPriceDiscount,(TUnitPrice - TUnitPriceDiscount) AS Saved_UnitPrice,
	 TCharge,TDiscountCharge,(TCharge - TDiscountCharge) AS Saved_TotalVale,'MMK' AS CURRENCY
FROM
(SELECT OrderID,
	   SUM(UnitPrice) AS TUnitPrice,
	   SUM(UnitPriceDiscount) AS TUnitPriceDiscount,
	   SUM(Charge) as TCharge,
	   SUM(DiscountCharge) as TDiscountCharge FROM
(SELECT A.OrderID,A.ProductID,A.UnitPrice,A.Quantity,A.Discount,
		(A.UnitPrice * A.Quantity) AS Charge,
		(A.UnitPrice - (A.UnitPrice * A.Discount)) AS UnitPriceDiscount,
		((A.UnitPrice - (A.UnitPrice * A.Discount)) * A.Quantity) AS DiscountCharge
FROM ivtb_order_details A)
GROUP BY OrderID)

-- CUSTOMER ORDER SUMMARY
CREATE VIEW ivtv_customer_order_summary AS
SELECT Customer,OrderAmount,'USD' AS CURRENCY FROM
(SELECT Customer,sum(NetAmount) AS OrderAmount  FROM
(SELECT A.OrderID,NetAmount AS NetAmount,B.CustomerID,C.CompanyName as Customer FROM
(SELECT OrderID,cast(sum(NetAmount) AS INTEGER) AS NetAmount FROM
(SELECT A.OrderID,A.UnitPrice,A.Quantity,A.Discount,((A.UnitPrice - (A.UnitPrice * A.Discount)) * A.Quantity) AS NetAmount
FROM ivtb_order_details A)
GROUP BY OrderID) A
LEFT JOIN (SELECT OrderID,CustomerID FROM ivtb_order_master) B
ON B.OrderID = A.OrderID
LEFT JOIN (SELECT CustomerID,CompanyName FROM ivtm_customers) C
ON B.CustomerID = C.CustomerID)
GROUP BY Customer)
ORDER BY OrderAmount DESC

-- Product ORDER SUMMARY
CREATE VIEW ivtv_prodcut_order_summary AS
SELECT ProductName,OrderFrequency,NetAmount FROM
(SELECT A.ProductID,A.NetAmount,A.OrderFrequency,B.ProductName FROM
(SELECT ProductID,cast(sum(NetAmount) AS INTEGER) AS NetAmount,count(*) AS OrderFrequency FROM
(SELECT A.ProductID,A.UnitPrice,A.Quantity,A.Discount,((A.UnitPrice - (A.UnitPrice * A.Discount)) * A.Quantity) AS NetAmount
FROM ivtb_order_details A
--WHERE ProductID = '11'
)
GROUP BY ProductID) A
LEFT JOIN (SELECT ProductID,ProductName from ivtb_products) B
ON A.ProductID = B.ProductID)
ORDER BY NetAmount DESC

-- CATEGORY ORDER SUMMARY
CREATE VIEW ivtv_categories_order_master AS
SELECT CategoryName,Description,sum(NetAmount) AS NetAmount,sum(OrderFrequency) AS OrderFrequency FROM
(SELECT A.ProductID,A.NetAmount,A.OrderFrequency,B.CategoryID,C.CategoryName AS CategoryName,C.Description AS Description FROM
(SELECT ProductID,cast(sum(NetAmount) AS INTEGER) AS NetAmount,count(*) AS OrderFrequency FROM
(SELECT A.ProductID,A.UnitPrice,A.Quantity,A.Discount,((A.UnitPrice - (A.UnitPrice * A.Discount)) * A.Quantity) AS NetAmount
FROM ivtb_order_details A
--WHERE ProductID = '11'
)
GROUP BY ProductID) A
LEFT JOIN (SELECT ProductID,CategoryID from ivtb_products) B
ON A.ProductID = B.ProductID
LEFT JOIN (SELECT CategoryID,CategoryName,Description FROM ivtm_categories) C
ON B.CategoryID = C.CategoryID)
GROUP BY CategoryName,Description






